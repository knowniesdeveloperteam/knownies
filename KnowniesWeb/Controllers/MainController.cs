﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KnowniesWeb.Models;
using KnowniesWeb.UserService;

namespace KnowniesWeb.Controllers
{
    public class MainController : Controller
    {
        //
        // GET: /Main/
        UserServiceClient oUserClient;

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(MainPageModel model)
        {
            oUserClient = new UserService.UserServiceClient();
            UserInfo user = oUserClient.AuthenticateUser(model.LoginForm.UserName, model.LoginForm.Password);

            if (user != null)
                return View("UserProfile", new MainPageModel { UserInfo = user, Result = new Result { IsCompleted = true } });
            else
                return View("Index", new MainPageModel { Result = new Result { IsCompleted = false, ErrorMessage = "Hatalı kullanıcı" } });
        }

        public ActionResult Signup()
        {
            return View("Signup");
        }

        public ActionResult SignupResult(T_USER user)
        {
            oUserClient = new UserServiceClient();
            user.CREATED_DATETIME = DateTime.Now;
            user.ROLE_ID = 1;
            Result oResult = null;
            MainPageModel oModel = new MainPageModel();
            if (user.CONFIRM_PASSWORD != user.PASSWORD)
                oResult = new Result { IsCompleted = false, ErrorMessage = "Şifre ve şifre tekrarı aynı değil." };
            else
            {
                oResult = oUserClient.CreateUser(user);
                oModel.UserInfo = new UserInfo { Name = user.NAME, Surname = user.SURNAME };
            }

            oModel.Result = oResult;

            if (oResult.IsCompleted)
                return View("UserProfile", oModel);
            else
                return View("Signup", oModel);
        }

        public ActionResult ForgetPassword() 
        {
            return View("Index", new MainPageModel { viewpart = "forgetpassword" });
        }

        public ActionResult ForgetPasswordResult(MainPageModel model)
        {
            oUserClient = new UserServiceClient();
            Result oResult = oUserClient.SendNewPasswordLink(model.RetrivePasswordKey);
            return View("Index", new MainPageModel { viewpart = "forgetpassword", Result = oResult });
        
        }

    }
}
