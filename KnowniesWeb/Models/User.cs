﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KnowniesWeb.UserService;

namespace KnowniesWeb.Models
{
    public class User: T_USER
    {
        [Compare("PASSWORD")]
        public string ConfirmPassword { get; set; }
        
    }
}