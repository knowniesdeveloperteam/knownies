﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowniesWeb.Models
{
    public class LoginForm
    {
        private string userName;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        private string errorMessage;

        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }
        
    }
}