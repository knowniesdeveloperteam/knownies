﻿using KnowniesWeb.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowniesWeb.Models
{
    public class MainPageModel
    {
        public LoginForm LoginForm { get; set; }
        public User UserParam { get; set; }
        public T_USER User { get; set; }
        public UserInfo UserInfo { get; set; }

        public Result Result { get; set; }
        public string viewpart { get; set; }

        public string RetrivePasswordKey { get; set; }
    }
}