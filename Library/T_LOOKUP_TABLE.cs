//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_LOOKUP_TABLE
    {
        public T_LOOKUP_TABLE()
        {
            this.T_LOOKUP_VALUE = new HashSet<T_LOOKUP_VALUE>();
        }
    
        public long RECORD_ID { get; set; }
        public string CODE { get; set; }
        public Nullable<long> CREATED_USER { get; set; }
        public System.DateTime CREATED_DATETIME { get; set; }
        public bool DELETED { get; set; }
        public Nullable<int> STATUS_ID { get; set; }
        public Nullable<bool> SYSTEM { get; set; }
        public Nullable<System.DateTime> MODIFIED_DATETIME { get; set; }
        public Nullable<long> MODIFIED_USER { get; set; }
        public string DESCRIPTION_1 { get; set; }
    
        public virtual ICollection<T_LOOKUP_VALUE> T_LOOKUP_VALUE { get; set; }
    }
}
