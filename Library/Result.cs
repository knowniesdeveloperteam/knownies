﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library
{
    public class Result
    {
        private bool isCompleted;

        public bool IsCompleted
        {
            get { return isCompleted; }
            set { isCompleted = value; }
        }

        private string errorMessage;

        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        
    }
}
