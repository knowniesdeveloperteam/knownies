﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Mail
    {
        public static void Send(MailMessage oMail)
        {
            oMail.From = new MailAddress("knownies.com@gmail.com", "knownies.com");
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(oMail.From.Address, "KNOWNIES_DEV")
            };

            smtp.Send(oMail);        
        }
    }
}
