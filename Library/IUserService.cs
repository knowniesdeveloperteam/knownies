﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Library
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IUserService : IBase
    {
        [OperationContract]
        Result CreateUser(T_USER user);
        [OperationContract]
        UserInfo AuthenticateUser(string username, string password);
        [OperationContract]
        bool UpdateUser(T_USER user);
        [OperationContract]
        string ChangePassword(string username, string oldPassword, string newPassword, string confirmPassword);

        [OperationContract]
        Result SendNewPasswordLink(string key);
    }




    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Library.ContractType".

}
