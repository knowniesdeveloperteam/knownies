﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Library
{ //test

    //deneme ebru NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class UserService : IUserService
    {
        KNOWNIES_DEVEntities kn = new KNOWNIES_DEVEntities();

        public System.Data.DataSet GetData(int RecordId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet GetSearch(string filter)
        {
            throw new NotImplementedException();
        }

        public UserInfo AuthenticateUser(string username, string password)
        {
            UserInfo oUser = null;
            T_USER user = new T_USER();
            user = kn.T_USER.Where(x => x.USER_NAME == username && x.PASSWORD == password).FirstOrDefault();
            if (user != null)
                oUser = new UserInfo() { Name = user.NAME, Surname = user.SURNAME, ImageUrl = user.PHOTO_ID.ToString() };

            return oUser;
        }

        public Result CreateUser(T_USER user)
        {
            try
            {
                if (kn.T_USER.Any(u => u.USER_NAME == user.USER_NAME))
                    throw new Exception("Bu kullanıcı adı mevcut");

                if (kn.T_USER.Any(u => u.EMAIL == user.EMAIL))
                    throw new Exception("Bu email mevcut");

                kn.T_USER.Add(user);
                kn.SaveChanges();

                return new Result() { ErrorMessage = string.Empty, IsCompleted = true };
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.ErrorMessage);
                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);
                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                // Throw a new DbEntityValidationException with the improved exception message.
                return new Result() { ErrorMessage = exceptionMessage, IsCompleted = false };
            }
            catch (Exception exp)
            {
                // Throw a new DbEntityValidationException with the improved exception message.
                return new Result() { ErrorMessage = exp.Message, IsCompleted = false };
            }
        }

        System.Data.DataSet IBase.GetData(int RecordId)
        {
            throw new NotImplementedException();
        }

        System.Data.DataSet IBase.GetSearch(string filter)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUser(T_USER user)
        {
            throw new NotImplementedException();
        }

        public string ChangePassword(string username, string oldPassword, string newPassword, string confirmPassword)
        {

            T_USER user = kn.T_USER.Where(x => x.USER_NAME == username && x.PASSWORD == oldPassword).FirstOrDefault();
            if (user != null)
            {
                if (oldPassword == newPassword)
                    return "Eski şifre ile aynı şifre girilemez.";

                if (newPassword != confirmPassword)
                    return "Yeni şifre ile yeni şifre tekrarı eşleşmiyor.";
                user.PASSWORD = newPassword;
                kn.SaveChanges();
                return "İşlem Başarılı";
            }

            return "Kullanıcı adı veya şifre hatalı";
        }

        public Result SendNewPasswordLink(string key)
        {
            Result oResult = new Result();
            try
            {
                string email = kn.T_USER.Where(u => u.USER_NAME == key || u.EMAIL == key).Select(x => x.EMAIL).FirstOrDefault();

                if (string.IsNullOrEmpty(email))
                    throw new Exception("Böyle bir kullanıcı yok.");

                System.Net.Mail.MailMessage oMail = new System.Net.Mail.MailMessage();
                oMail.Body = "şifre değiştirme linki";
                oMail.To.Add(email);
                oMail.From = new System.Net.Mail.MailAddress("knownies.com@gmail.com", "knownies.com");
                oMail.Subject = "Şifre Değiştirme";
                Mail.Send(oMail);

                oResult.IsCompleted = true;
            }
            catch (Exception ex)
            {
                oResult.IsCompleted = false;
                oResult.ErrorMessage = "Mail gönderimi başarısız. " + ex.Message;
            }
            return oResult;
        }
    }
}
