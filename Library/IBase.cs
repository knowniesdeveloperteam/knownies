﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
  public  interface IBase 
    {
        
         DataSet GetData(int RecordId);
       
          DataSet GetSearch(string filter);

    }
}
